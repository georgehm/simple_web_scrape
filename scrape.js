let classKnwl = require("knwl.js");
let modRp = require("request-promise");
let modKnwl = new classKnwl("english");


function getDomain (callback) { 
   return new Promise((resolve, reject) => {

       let arrComDomains = [
            "aol.com",  
            "gmail.com",
            "googlemail.com",
            "hotmail.com",
            "live.co.uk",
            "outlook.com",
            "yahoo.com"
        ];

        let strDomain = process.argv[2];
        let rgxValidator = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!strDomain || !strDomain.match(rgxValidator)) {
            return reject("Missing email argument or not in email format.");
        }

        strDomain = strDomain.split("@")[1];
        if (arrComDomains.includes(strDomain)) {
            return reject("Common domain detected. Stopping.");
        }
        return resolve(strDomain);
    });
}

getDomain().then(strDomain => {
    let objOptions = {
        uri: "http://www." + strDomain,
        method: "GET"
    };

    console.log("Trying to load", strDomain);
    
    // make a request for the website    
    modRp(objOptions).then(data => {
        data = data.replace(/<[^>]+>/gi, " ");
        modKnwl.init(data);

        let arrSteps = [
            "phones",
            "links",
            "emails",
            "places"
        ];

        // this is the object we are going to return
        // will insert data into it as we go
        let objReturnData = {
            website: strDomain,
            phones: [],
            emails: [],
            places: []
        };

        let arrKnwlData;
        arrSteps.forEach(step => {
            // go through each process and get it from knwl
            arrKnwlData = modKnwl.get(step);

            // go through the array we get from knwl (each one is an object)
            arrKnwlData.forEach(result => {
                // get the first key from the object
                let strKeyData = result[Object.keys(result)[0]];

                if (!objReturnData[step].includes(strKeyData)) {
                    objReturnData[step].push(strKeyData);
                }
            });
        });

        return console.log(JSON.stringify(objReturnData, null, 2));
    }).catch(err => {
        return console.log(err);
    });

}).catch(err => {
    console.log(err);
});
