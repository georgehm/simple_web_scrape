node.js script to scrape various data from a website

The website is decided based on the domain name of an email adddress
provided via the CLI

Example usage:
`node scrape.js some_person@google.com`

Uses knwl.js for scraping.

Dependencies:
- knwl.js
- request
- request-promise